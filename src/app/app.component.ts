import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Tiendas', url: '/folder/Tiendas', icon: 'barcode' },
    { title: 'Mis compras', url: '/folder/Mis compras', icon: 'podium' },
    { title: 'Tiendas Favoritas', url: '/folder/Tiendas favoritas', icon: 'heart' },
    { title: 'Mi Carrito', url: '/folder/Mi carrito', icon: 'car' },
  ];
  objetoapp ={
    app : {
      titulo: "Shoes App"
    },
  };
  objetoPersona ={
    id: 11111,
    city :"Barranquilla",
    addres :{
      street: "10",
      secund: "50",
      number: "55"
    },
  };
  data : any;
  labels = [ 'Reportar un error', 'Terminos y condiciones'];
  constructor() {}
}
